Overview
========

This project builds and hosts a Docker Image that provides an Apache reverse proxy
that does Single Sign-On authentication. The image built is accessible from the
rest of the OpenShift projects so any can use the proxy.

Deployment of the ImageStream
========

This image is intended to be deployed as an externally managed resource. Check the
internal documentation in OneNote for instructions on how to do it.

Deploy templates:
----------------

The templates will be automatically deployed with gitlab-CI when they are changed. To deploy on prod,
change the template on branch `master`


Usage:
-----
Using the template will result on the creation of a Pod spawning two containers,
running `httpd` and `shibd` respectively, that will proxy connection through a route to
other sevice running the project. By default, the proxy will require authentication for
every path on the route.

There are two ConfigMaps where you can define custom files that will be injected into
the containers.

- `cern-sso-proxy` : This ConfigMap contains files that will be copied to `/etc/httpd/conf/configurable/`
and included in the httpd configuration on runtime. By default, two files are present, `proxy.conf` and `authorize.conf`
that define a single proxy of every route in the path

- `cern-sso-shib`: This ConfigMap contains files that will be copied to `/etc/shibboleth/`. It includes the configuration
for shibd daemon and algo the `shib_httpd.conf` apache configuration that also lives in this folder. It is very unlikely that
you will need to change something here.


IMPORTANT:
----
This template will  define a single entrypoint for you application, which may conflict with existing routes on the project. Therefore,
it is recommended to remove all the routes that may already exist in the project before using the template. To do so, you can run:
```
oc delete routes --all -n <name-of-your-project>
```

Custom DNS names
----------------

By default the proxy configuration assumes the webservices-assigned DNS name
(`<sitename>.web.cern.ch`). In specific cases where a different DNS name is used,
it needs to be passed to the proxy. An environment variable can be set to achieve this:

```bash
oc env dc/cern-sso-proxy --containers='*' HOSTNAME_FQDN=foo.cern.ch --overwrite
```

The [route](https://docs.openshift.org/latest/dev_guide/routes.html) then needs to be reconfigured
with appropriate hostname and certificate.

Set the backend service from environment
----------------------------

Environment variables can be used to override the backend service to proxy. 
This allows to proxy services running outside Openshift or in different Openshift
projects (see also [KB0005442](https://cern.service-now.com/service-portal/article.do?n=KB0005442)).

```bash
oc env dc/cern-sso-proxy --containers=httpd SERVICE_HOST=mybackend.cern.ch SERVICE_PORT=80 --overwrite
```


Continuous integration tests
----------------------------
As part of the Continuous Integration jobs, both the newly built image and the template
are tested in two OpenShift projects, created specifically for this task in the [dev cluster](https://openshift-dev.cern.ch).
* [test-cern-sso-proxy](https://openshift-dev.cern.ch/console/project/test-cern-sso-proxy): Create a new SSO proxy
using the new built image and template and wait until it redirects to `login.cern.ch`
* [test-cern-sso-proxy-2](https://openshift-dev.cern.ch/console/project/test-cern-sso-proxy-2): Create a new SSO proxy
using the new template as-is (will use the `stable` images) and wait until it redirects to `login.cern.ch`

These 2 projects are created as normal webservices applications, then we need to grant
admin access to the serviceaccount associated with the cern-sso-proxy resource/template:

```
oc adm policy add-role-to-user admin system:serviceaccount:openshift:cern-sso-proxy -n test-cern-sso-proxy
oc adm policy add-role-to-user admin system:serviceaccount:openshift:cern-sso-proxy -n test-cern-sso-proxy-2
```
